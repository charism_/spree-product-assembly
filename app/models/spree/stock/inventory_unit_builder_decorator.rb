module Spree
  module Stock
    module InventoryUnitBuilderDecorator
      def units
        @order.line_items.flat_map do |line_item|
          if line_item.product.assembly? && line_item.part_line_items.any?
            line_item.part_line_items.map do |part_line_item|
              build_inventory_unit(
                part_line_item.variant,
                line_item,
                part_line_item.quantity * line_item.quantity
              )
            end
          else
            line_item.quantity_by_variant.flat_map do |variant, quantity|
              build_inventory_unit(variant, line_item, quantity)
            end
          end
        end
      end

      def build_inventory_unit(variant, line_item, quantity)
        Spree::InventoryUnit.new(
          pending: true,
          line_item: line_item,
          variant: variant,
          order: @order,
          quantity: quantity
        )
      end
    end
  end
end

Spree::Stock::InventoryUnitBuilder.prepend Spree::Stock::InventoryUnitBuilderDecorator
